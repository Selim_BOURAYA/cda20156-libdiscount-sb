package fr.afpa.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.User;
import fr.afpa.constants.Constants;
import fr.afpa.model.InstanceUser;
import junit.framework.TestCase;

public class AnnonceTest extends TestCase implements Constants {
	
	private Annonce annonce;
	private User user;
	private InstanceUser instUser;

	@Before
	protected void setUp() throws Exception {
		super.setUp();
		annonce = new Annonce();
		user = new User("test", "test", "test", "test", "test", "test",	"test", "test", "login", "password", "test");
		instUser = new InstanceUser();
				
	}
	
	
	public void testCreateUser() {
		String verif = instUser.buildUserBDD("test", "test", "test", "test", "test", "test", "test", "test", "login", "password", "test");
		assertEquals(errorPage, verif);
	}
	
	public void testAuthFalseUser() {
		String verif = instUser.CheckUserBDD(user.getLogin(), user.getPassword());
		assertEquals(mainPageAdmin, verif);
	}
			
	public void testCreateAdminUser() {
		String verif = instUser.buildUserBDD("testAdmin", "testAdmin", "testAdmin", "testAdmin", "testAdmin", "testAdmin", "testAdmin", "testAdmin", "Mike", "1234", "admin");
		assertEquals(mainPageAdmin, verif);
	}




	@After
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void test() {
		fail("Not yet implemented");
	}

}
