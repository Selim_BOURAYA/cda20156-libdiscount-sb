<%@ page language="java" contentType="text/html; charset=utf8"
    pageEncoding="utf-8" isELIgnored="false"%>
<!DOCTYPE html>
 <meta charset="utf-8">
       <title>Accueil - LeBonLibraire</title>
        <!-- importer le fichier de style -->
       <link href="style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
    
    	<% int id = Integer.parseInt((String)session.getAttribute("id"));%>
   
        <div id="container">           
            
            <form action="ServletUserAnnonce" method ="get">
            <h1 id="titre" >Le Bon Libraire</h1>
              
               <a href=ServletUserAnnonce?id=<%=id%>>Consulter vos annonces</a>

        	</form>
            <form action="ServletUserPage" method ="post">
                                
                <input type="submit" value="Ajouter une annonce" name="addAnnonce" required>
                               
                <input type="submit" value="Rechercher des annonces" name="findAnnonces" required>
                
            </form>
        </div>
        
        <div>
        <form action = "ServletAccueil" method ="post">
        <input type="submit" value="se déconnecter" name="deconnexion" required>
        </form>
        </div>
        
    </body>
</html>