<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page import="fr.afpa.beans.Annonce"%>
<%@ page import="java.util.ArrayList"%>

<!DOCTYPE html>
<html>
<link href="style.css" rel="stylesheet" type="text/css">
<head>
<meta charset="ISO-8859-1">

<title>Liste des annonces</title>
</head>
<body>

	<% ArrayList <Annonce> listAnnonce = (ArrayList <Annonce>)request.getAttribute("listeAnnonce"); %>

	<% for(Annonce annonce : listAnnonce){ 
		
			if(annonce.getEtat().contentEquals("active")) {%>

	<form method="post" action="ServletAddAnnonce">
	<table>
		<tr>
			<td style="text-align: center;"><img src="Resources/livre.png">  <%=annonce.getTitre()%>   <%=annonce.getEditeur()%>    <%=annonce.getAnnee()%>   <%=annonce.getQuantite()%>    <%=annonce.getRemise()%>    <%=annonce.getPrix()%>   <%=annonce.getNiveau()%> </td>
			
			<td style="text-align: center;"><a href=ServletDeactivAnnonce?role=user&id=<%=annonce.getIdannonce()%>> désactiver l'annonce</a></td>
			<td style="text-align: center;"><a href=ServletUserPage?id=<%=annonce.getIdannonce()%>> modifier annonce</a></td>
		</tr>
	</table>
	</form>
	<% } 
			
			else {%>

	<table>
		<tr>
			<td style="text-align: center;"><%=annonce.getTitre()%>   <%=annonce.getEditeur()%>    <%=annonce.getAnnee()%>   <%=annonce.getQuantite()%>    <%=annonce.getRemise()%>    <%=annonce.getPrix()%>   <%=annonce.getNiveau()%></td>
			
			<td style="text-align: center;"><a href=ServletReactivAnnonce?role=user&id=<%=annonce.getIdannonce()%>> reactiver l'annonce</a></td>
		</tr>
	</table>

	<% } 
			
		} %>
		
		<form action ="ServletUserPage" method = "post">
		<input type="submit" name = "accueil" value = "Retour à l'accueil">
	</form>

</body>
</html>