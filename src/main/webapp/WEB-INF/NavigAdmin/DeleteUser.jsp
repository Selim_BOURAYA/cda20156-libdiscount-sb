<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>
    <%@ page import="fr.afpa.beans.User"%> 
<%@ page import="java.util.ArrayList"%> 
    
<!DOCTYPE html>
<html>
<link href="style.css" rel="stylesheet" type="text/css">
<head>
<meta charset="UTF-8">
<title> Gestion des utilisateurs </title>
</head>
<body>

	<% ArrayList <User> listUser = (ArrayList <User>)request.getAttribute("listeUser"); %>
	
	<% for(User user : listUser){
		
		if(user.getStatut().contentEquals("actif")) {%>

	<table>
		<tr>
			<td style="text-align: center;"><%=user.getNom()%> <%=user.getPrenom()%> <%=user.getLibrairie()%>  <%=user.getVille()%>   <%=user.getLogin()%>    <%=user.getLogin()%>    <%=user.getStatut()%>  <%=user.getRole()%>  <%=user.getLogin()%></td>
			<td style="text-align: center;"><a href=ServletDelUser?login=<%=user.getLogin()%>>   désactiver utilisateur</a></td>
		</tr>
	</table>

		<%} else if (user.getStatut().contentEquals("inactif")) {%>
		
		<table>
			<tr>
				<td style="text-align: center;"><%=user.getNom()%> <%=user.getPrenom()%> <%=user.getLibrairie()%>  <%=user.getVille()%>   <%=user.getLogin()%>    <%=user.getLogin()%>    <%=user.getStatut()%>  <%=user.getRole()%></td>
				<td style="text-align: center;"><a href=ServletReactivUser?login=<%=user.getLogin()%>>   réactiver utilisateur</a></td>
			</tr>
		</table>

		<%}
	}%>
	
	<form action ="ServletAdminPage" method = "post">
		<input type="submit" name = "accueilAdmin" value = "Retour à l'accueil">
	</form>

</body>
</html>