<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="fr.afpa.beans.Annonce"%>
<%@ page import="java.util.ArrayList"%>

<!DOCTYPE html>
<html>
<link href="style.css" rel="stylesheet" type="text/css">
<head>
<meta charset="ISO-8859-1">

<title>Liste des annonces</title>
</head>
<body>

	<% ArrayList <Annonce> listAnnonce = (ArrayList <Annonce>)request.getAttribute("listeAnnonce"); %>

	<% for(Annonce annonce : listAnnonce){ 
		
			if(annonce.getEtat().contentEquals("active")) {%>

	<table>
		<tr>
			<td style="text-align: center;"><%=annonce.getTitre()%>   <%=annonce.getEditeur()%>    <%=annonce.getAnnee()%>   <%=annonce.getQuantite()%>    <%=annonce.getRemise()%>    <%=annonce.getPrix()%>   <%=annonce.getNiveau()%></td>
			
			<td style="text-align: center;"><a href=ServletDeactivAnnonce?id=<%=annonce.getIdannonce()%>> d�sactiver l'annonce</a></td>
			
			<td style="text-align: center;"><a href=ServletDelAnnonce?id=<%=annonce.getIdannonce()%>> supprimer l'annonce</a></td>
		</tr>
	</table>

	<% } 
			
			else {%>

	<table>
		<tr>
			<td style="text-align: center;"><%=annonce.getTitre()%>   <%=annonce.getEditeur()%>    <%=annonce.getAnnee()%>   <%=annonce.getQuantite()%>    <%=annonce.getRemise()%>    <%=annonce.getPrix()%>   <%=annonce.getNiveau()%></td>
			
			<td style="text-align: center;"><a href=ServletReactivAnnonce?id=<%=annonce.getIdannonce()%>> reactiver l'annonce</a></td>
		</tr>
	</table>

	<% } 
			
		} %>
		
		<form action ="ServletAdminPage" method = "post">
		<input type="submit" name = "accueilAdmin" value = "Retour � l'accueil">
	</form>

</body>
</html>