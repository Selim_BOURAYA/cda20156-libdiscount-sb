package fr.afpa.constants;

public interface Constants {
	
	//PAGES ADMINISTRATEUR
	public final String mainPageAdmin = "WEB-INF/NavigAdmin/MainPageAdmin.html";
	public final String addUser = "WEB-INF/NavigAdmin/addUser.html";
	public final String addUserSucceed = "WEB-INF/NavigAdmin/AddUserSucceed.html";
	public final String deactivatedSucceed = "WEB-INF/NavigAdmin/DeactivatedSucceed.html";
	public final String activatedSucceed = "WEB-INF/NavigAdmin/ActivatedSucceed.html";
	
	public final String deleteUser = "WEB-INF/NavigAdmin/DeleteUser.jsp";
	
	public final String deleteAnnonce = "WEB-INF/NavigAdmin/DeleteAnnonce.jsp";
	public final String listAnnonce = "WEB-INF/NavigAdmin/listAnnonce.jsp";
		
	//PAGES UTILISATEURS
	public final String addNewUser = "WEB-INF/NavigUser/addNewUser.html";
	public final String mainPageUser = "WEB-INF/NavigUser/MainPageUser.jsp";
	public final String addAnnonce = "WEB-INF/NavigUser/addAnnonce.html";
	public final String addAnnonceSucceed = "WEB-INF/NavigUser/AddAnnonceSucceed.html";
	public final String modifAnnonce = "WEB-INF/NavigUser/modifierAnnonce.jsp";
	public final String AnnonceUserDeactivated = "WEB-INF/NavigUser/AnnonceUserDeactivated.html";
	public final String AnnonceUserActivated = "WEB-INF/NavigUser/AnnonceUserActivated.html";
	public final String triAnnonce = "WEB-INF/NavigUser/triAnnonce.jsp";
	public final String listUserAnnonce = "WEB-INF/NavigUser/listUserAnnonce.jsp";
	public final String annonceFiltrees = "WEB-INF/NavigUser/annonceFiltrees.jsp";
	
	//AUTRES
	public final String errorPage = "WEB-INF/error.html";
	public final String authentification = "index.jsp";
	
}
