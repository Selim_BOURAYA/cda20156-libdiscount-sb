package fr.afpa.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.User;
import fr.afpa.model.InstanceUser;
import fr.afpa.session.HibernateUtils;
import lombok.Getter;
import lombok.ToString;

public class CommBDD {

	private User user;
	private static User loggedUser;
	private static String loginUser;
	private int idUser;
	private InstanceUser buildUser;
	private Session session;
	private Transaction transaction;
	private Query query;
	private String statut;
	private String id;

	
	
	// METHODES USER //
	
	/**
	 * @author selim
	 * @param enregistrement de l'utilisateur dans la base de données
	 */
	public void createUser(User user) {

		// ouverture de la session hibernate et d'une instance de transaction
		session = HibernateUtils.getSession();
		transaction = session.beginTransaction();

		session.persist(user);
		session.merge(user);
		transaction.commit();
		session.close();
	}

	
	/**
	 * @author selim
	 * @param fonction booleenne verifiant la correspondance entre le login et le
	 *                 password de l'User et ceux enregistrés dans la BDD
	 * 
	 * @param texte
	 */
	public boolean checkValidUser(String login, String password) {

		// ouverture de la session hibernate et d'une instance de transaction
		session = HibernateUtils.getSession();
		transaction = session.beginTransaction();
		
		query = session.getNamedQuery("findByLogin");
		query.setParameter("login",login);
		
		ArrayList<User> listeUser = (ArrayList<User>) query.getResultList();
		
		for (User user : listeUser) {
			
			if (user.getPassword().equals(password)) {
				// on enregistre le login du client connecté
				loginUser = login;
				buildUser = new InstanceUser();
				// on appelle la méthode "getUser" afin de reconstruire et enregistrer l'objet
				// correspondant au client connecté
				loggedUser = buildUser.getUser(login);
				loggedUser.setIduser(user.getIduser());
				session.close();
				return true;
			}
		}
		session.close();
		return false;
	}

	
	/**
	 * @author selim
	 * @param login methode permettant, pour un login donné, de récuperer
	 *              l'utilisateur
	 */
	public User getUserBDD(String login) {

		session = HibernateUtils.getSession();
		transaction = session.beginTransaction();

		query = session.getNamedQuery("findByLogin");
		query.setParameter("login", login);

		ArrayList<User> listeUser = (ArrayList<User>) query.getResultList();

		if (listeUser != null && listeUser.size() == 1) {

			return listeUser.get(0);
		}
		session.close();
		return null;
	}
	
	
	/**
	 * @author selim
	 * methode permettant de récupérer la liste de utilisateurs, à l'exception des administrateurs, presents dans la table UserLib
	 */
	public ArrayList <User> getListUserBDD() {
		
		session = HibernateUtils.getSession();
		transaction = session.beginTransaction();
		
		query = session.getNamedQuery("findByRole");
		query.setParameter("role", "user");
		
		ArrayList<User> listeUser = (ArrayList<User>) query.getResultList();
		
		if(listeUser != null) {
		return listeUser;
		}
		session.close();
		return null;
	}
	
	
	/**
	 * @author selim
	 * methode permettant de d'intervertir, en fonction de l'état d'entrée, le statut de l'utilisateur (actif / inactif)
	 */
	public String setInactiveUser(String login) {
		
		session = HibernateUtils.getSession();
		transaction = session.beginTransaction();
		
		query = session.getNamedQuery("findByLogin");
		query.setParameter("login", login);
		
		ArrayList<User> listeUser = (ArrayList<User>) query.getResultList();
		
		if (listeUser != null && listeUser.size() == 1 && listeUser.get(0).getStatut().contentEquals("actif")) {
			
			listeUser.get(0).setStatut("inactif");
			
			session.persist(listeUser.get(0));
			session.merge(listeUser.get(0));
			
			transaction.commit();
			session.close();
			
			statut = "inactif";
						
			return statut;
		}
	
		else if (listeUser != null && listeUser.size() == 1 && listeUser.get(0).getStatut().contentEquals("inactif")) {
			
			listeUser.get(0).setStatut("actif");
			
			session.persist(listeUser.get(0));
			session.merge(listeUser.get(0));
			
			transaction.commit();
			session.close();
			
			statut = "actif";
			
			return statut;
		}		
		
		session.close();
		return null;
	}
	
	
	/**
	 * @author selim
	 * @param login methode permettant, pour un login donné, de récuperer l'id de
	 *              l'utilisateur
	 */
	public int idUser(String login) {

		session = HibernateUtils.getSession();
		transaction = session.beginTransaction();

		query = session.getNamedQuery("findByLogin");
		query.setParameter("login", login);

		ArrayList<User> listeUser = (ArrayList<User>) query.getResultList();

		for (User user : listeUser) {

			idUser = user.getIduser();
			session.close();
			return idUser;
		}
		return idUser;
	}
	
		
	
	// METHODES ANNONCES //
	
	
	
	/**
	 * @author selim
	 * @param enregistrement d'une annonce dans la base de données
	 * 
	 */
	public void createAnnonce(Annonce annonce) {

		// ouverture de la session hibernate et d'une instance de transaction
		session = HibernateUtils.getSession();
		transaction = session.beginTransaction();

		// on recupère l'état de l'utilisateur connecté
		buildUser = new InstanceUser();
		user = buildUser.getUser(loginUser);
		annonce.setUser(user);

		session.persist(annonce);
		session.merge(annonce);
		transaction.commit();
		session.close();
	}
	
	/**
	 * @author selim
	 * @param remplacement d'une annonce dans la base de données
	 * 
	 */
	public void replaceAnnonce(Annonce annonce, int idannonce) {
		
		// ouverture de la session hibernate et d'une instance de transaction
		session = HibernateUtils.getSession();
		transaction = session.beginTransaction();
		
		// on recupère l'état de l'utilisateur connecté
		buildUser = new InstanceUser();
		user = buildUser.getUser(loginUser);
		annonce.setUser(user);
		annonce.setIdannonce(idannonce);
		
		session.merge(annonce);
		transaction.commit();
		session.close();
	}

	
	/**
	 * @author selim 
	 * methode de recupération des l'ensemble des annonces enregistrées dans la table Annonce
	 */
	public ArrayList<Annonce> getAllAnnonceBDD() {

		session = HibernateUtils.getSession();
		transaction = session.beginTransaction();

		query = session.getNamedQuery("getAllAnnonces");
		
		ArrayList<Annonce> listeAnnonce = (ArrayList<Annonce>) query.getResultList();
		
			session.close();
			return listeAnnonce;
	}
	
	
	/**
	 * @author selim 
	 * methode de recupération des l'ensemble des annonces enregistrées dans la table Annonce
	 */
	public ArrayList<Annonce> getOwnAnnonceBDD(String id) {
		
		session = HibernateUtils.getSession();
		transaction = session.beginTransaction();
		System.out.println("id : "+id);
		
		query = session.getNamedQuery("getAllAnnonces");
				
		ArrayList<Annonce> listeAnnonce = (ArrayList<Annonce>) query.getResultList();
		ArrayList <Annonce> newListeAnnonce = new ArrayList<Annonce>();
		
		for (Annonce annonce : listeAnnonce) {
			
			if (annonce.getUser().getIduser() == (Integer.parseInt(id))) {
				
				newListeAnnonce.add(annonce);
			}
		}
				
		session.close();
		return newListeAnnonce;
	}
	
	
	/**
	 * @author selim 
	 * methode de recupération des annonces grâce à des critéres de tri (titre,
	 *         niveau, editeur, ville, 
	 */
	public ArrayList<Annonce> getSelectedAnnonceBDD(String entry) {

		session = HibernateUtils.getSession();
		transaction = session.beginTransaction();

		query = session.getNamedQuery("getAllAnnonces");
		
		ArrayList <Annonce> listeAnnonce = (ArrayList<Annonce>) query.getResultList();
		ArrayList <Annonce> newListeAnnonce = new ArrayList<Annonce>();

		for (Annonce annonce : listeAnnonce) {
			
			id = ""+annonce.getIdannonce();

			if (annonce.getTitre().contentEquals(entry) || annonce.getAnnee().contentEquals(entry) 
					|| annonce.getEditeur().contentEquals(entry)|| annonce.getNiveau().contentEquals(entry) 
					|| annonce.getUser().getVille().contentEquals(entry) || id.contentEquals(entry)) {

				newListeAnnonce.add(annonce);
			}
		}
		session.close();
		return newListeAnnonce;
	}
	
	
	/**
	 * @author selim
	 * méthode qui intervertit l'etat de l'annonce, la faisant passer de "active" à "inactive",
	 * et vice-versa, en récupérant son état dans la table annonce puis en l'inversant.
	 * La méthode renvoie une réponse une chaine de caractère indiquant le nouvel état de l'annonce
	 * @retur
	 */
	public String setInactiveAnnonce(int id) {
		
		session = HibernateUtils.getSession();
		transaction = session.beginTransaction();
		
		query = session.getNamedQuery("findById");
		query.setParameter("idannonce", id);
		
		ArrayList<Annonce> listeAnnonce = (ArrayList<Annonce>) query.getResultList();
		
		if (listeAnnonce.get(0).getEtat().contentEquals("active")) {
			
			listeAnnonce.get(0).setEtat("inactive");
			
			session.merge(listeAnnonce.get(0));
			
			transaction.commit();
			session.close();
			
			statut = "inactive";
			System.out.println("inactive");
			
			return statut;
		}
		
		else if (listeAnnonce.get(0).getEtat().contentEquals("inactive")) {
			
			listeAnnonce.get(0).setEtat("active");
			
			session.merge(listeAnnonce.get(0));
			
			transaction.commit();
			session.close();
			
			statut = "active";
			System.out.println("active");
			
			return statut;
		}		
		
		session.close();
		return null;
	}
	
	
	/**
	 * @author selim
	 * methode permettant de supprimer une annonce dans la table Annonce
	 */
	public void deleteAnnonceBDD(int id) {
		
		session = HibernateUtils.getSession();
		transaction = session.beginTransaction();
		
		query = session.getNamedQuery("findById");
		query.setParameter("idannonce", id);
		
		ArrayList<Annonce> listeAnnonce = (ArrayList<Annonce>) query.getResultList();
		
		for (Annonce annonce : listeAnnonce) {
			System.out.println(annonce);
			session.delete(annonce);
			session.flush();
			session.clear();
			transaction.commit();
			session.close();
		}
	}

}