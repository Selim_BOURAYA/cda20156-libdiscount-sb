package fr.afpa.control;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.constants.Constants;
import fr.afpa.model.InstanceUser;

/**
 * Servlet implementation class UiAccueil
 */
public class ServletAccueil extends HttpServlet implements Constants {
	private static final long serialVersionUID = 1L;
	
	private String login;
	private String password;
	private InstanceUser instUser;
	private String nextView;
	private RequestDispatcher dispatcher;
	private HttpSession session;
	       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletAccueil() {
        super();
        
    }

	/**@author Selim
	 * lorsque le bouton "nouvel utilisateur est activé, la méthode appele la page "addNewUser"
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if (request.getParameter("newUser") != null) {
			
			nextView = addNewUser;
			
		}
			
		RequestDispatcher dispatcher = request.getRequestDispatcher(nextView);
		dispatcher.forward(request, response);
						
			
	}
	
	/**@author Selim
	 * Servlet gérant l'authentifcation et permettant de faire transiter le login et le password vers la couche métier
	 * qui aura la charge d'effectuer le controle auprès de la couche DAO
	 * 
	 * @
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		if(request.getParameter("login") != null && request.getParameter("password") != null && request.getParameter("addUser") == null) {
		
		login = request.getParameter("login");
		password = request.getParameter("password");
							
		instUser = new InstanceUser();
		nextView = instUser.CheckUserBDD(login, password);
			
			// si la couche métier renvoi l'adresse de la page d'accueil utilisateur ou admin, une session est créée, et les attributs login, id et role sont renseignés
			if (nextView.contentEquals(mainPageUser) || nextView.contentEquals(mainPageAdmin)) {
				
				session = request.getSession();
				session.setAttribute("login", login);
				session.setAttribute("id", ""+instUser.getLoggedUser().getIduser());
				session.setAttribute("role", ""+instUser.getLoggedUser().getRole());
				
			}
			else { nextView = errorPage;}
				
		dispatcher = request.getRequestDispatcher(nextView);
		dispatcher.forward(request, response);
		
		}
		
		//retour vers l'authentification lorsque le bouton déconnexion est activé depuis la page d'accueil (user et admin)
		if (request.getParameter("deconnexion") != null && session != null) {
			
			session.invalidate();
			nextView = authentification;
			dispatcher = request.getRequestDispatcher(nextView);
			dispatcher.forward(request, response);
		}	
		
	}

}
