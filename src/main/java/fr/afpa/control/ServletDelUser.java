package fr.afpa.control;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import fr.afpa.model.InstanceUser;

/**
 * Servlet implementation class ServletDelUser
 */
public class ServletDelUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String login;
	private String nextView;
	private InstanceUser instUser;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletDelUser() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**@author Selim
	 * grace au login, récupérée depuis la page .jsp, l'utilisateur est supprimé de la base de données
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		login = request.getParameter("login");
		System.out.println(login);
					
		instUser = new InstanceUser();
		nextView = instUser.deactivatedUser(login);
			
		RequestDispatcher dispatcher = request.getRequestDispatcher(nextView);
		dispatcher.forward(request, response);
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
