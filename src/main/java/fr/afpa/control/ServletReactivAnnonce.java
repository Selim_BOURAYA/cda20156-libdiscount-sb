	package fr.afpa.control;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.model.InstanceAnnonce;

/**
 * Servlet implementation class ServletReactivAnnonce
 */
public class ServletReactivAnnonce extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private RequestDispatcher dispatcher;
	private int idAnnonce;
	private String role;
	private InstanceAnnonce instAnnonce;
	private String nextView;   
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletReactivAnnonce() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**@author Selim
	 * Grace à l'idannonce récupérée et au role de l'utilisateur, les annonces peuvent être réactivées depuis leurs interfaces respectives
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		idAnnonce = Integer.parseInt(request.getParameter("id"));
		role = request.getParameter("role");
				
		instAnnonce = new InstanceAnnonce();
		nextView = instAnnonce.deactivateAnnonce(idAnnonce, role);
				
		dispatcher = request.getRequestDispatcher(nextView);
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	

	}

}
