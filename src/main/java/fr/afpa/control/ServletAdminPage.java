package fr.afpa.control;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.constants.Constants;
import fr.afpa.model.InstanceAnnonce;
import fr.afpa.model.InstanceUser;

/**
 * Servlet implementation class ServletAddUser
 */
public class ServletAdminPage extends HttpServlet implements Constants {
	private static final long serialVersionUID = 1L;
	private String nextView;
	private InstanceUser instUser;
	private InstanceAnnonce instAnnonce;
	private ArrayList listeUser;
	private ArrayList listeAnnonce;
	
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletAdminPage() {
        super();
        // TODO Auto-generated constructor stub
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	/**@author Selim
	 * récupére les valeurs des inputs de l'interface admin et permet l'appel des pages (html ou jsp) souhaitées
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			
		// bouton "retour à l'accueil"
		if (request.getParameter("accueilAdmin") != null) {
			
			nextView = mainPageAdmin;
			
		}
		
		// bouton "ajouter un utilisateur"
		if (request.getParameter("addUser") != null) {
			
			nextView = addUser;
						
		}
		
		//bouton consulter la liste annonce
		if (request.getParameter("listAnnonce") != null) {
			
			instAnnonce = new InstanceAnnonce();
			listeAnnonce = instAnnonce.getAllAnnonce();
			
			request.setAttribute("listeAnnonce", listeAnnonce);
			nextView = listAnnonce;
			
		}
		
		// bouton "supprimer une annonce"
		if (request.getParameter("deleteAnnonce") != null) {
			
			nextView = deleteAnnonce;
			
		}
		
		//fonction de suppressionb d'un utilisateur
		if (request.getParameter("deleteUser") != null) {
			
			instUser = new InstanceUser();
			listeUser = instUser.getListUser();
			
			request.setAttribute("listeUser", listeUser);
			nextView = deleteUser;
						
		}
		
		
		RequestDispatcher dispatcher = request.getRequestDispatcher(nextView);
		dispatcher.forward(request, response);
		
		
	}

}
