package fr.afpa.control;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.constants.Constants;
import fr.afpa.model.InstanceAnnonce;
import fr.afpa.model.InstanceUser;

/**
 * Servlet implementation class ServletAddAnnonce
 */
public class ServletAddAnnonce extends HttpServlet implements Constants {
	private static final long serialVersionUID = 1L;
	private String titre;
	private String niveau;
	private String isbn;
	private String annee; 
	private String editeur;
	private int prix; 
	private int quantite; 
	private int remise;
	private String idannonce;
	private InstanceAnnonce instAnnonce;
	private RequestDispatcher dispatcher;
	private String nextView;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletAddAnnonce() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	}

	/**@author Selim
	 * Méthode récupérant les informations renseignées par l'utilisateur afin de les faire transiter vers la couche métier
	 * et instancier une nouvelle annonce
	 *
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		titre = request.getParameter("titre");
		niveau = request.getParameter("niveau");
		isbn = request.getParameter("isbn");
		annee = request.getParameter("annee"); 
		editeur = request.getParameter("editeur");
		prix = Integer.parseInt(request.getParameter("prix")); 
		quantite = Integer.parseInt(request.getParameter("quantite")); 
		remise = Integer.parseInt(request.getParameter("remise"));
		idannonce = request.getParameter("idannonce");
		
		// si l'idannonce est nulle (nouvelle annonce), on fait tranister les attributs récupérés vers la méthode buildAnnonceBDD
		if (titre != null && niveau != null && isbn != null && annee != null && editeur != null && 
				prix != 0 && quantite != 0 && idannonce == null) {
			
		instAnnonce = new InstanceAnnonce();
		nextView = instAnnonce.buildAnnonceBDD(titre, niveau, isbn, annee, editeur, prix, quantite, remise);
		
		}
		
		// si l'idannonce n'est pas nulle (modification d'annonce), on fait tranister les attributs récupérés vers la méthode changeAnnonceBDD
		else if (titre != null && niveau != null && isbn != null && annee != null && editeur != null && 
				prix != 0 && quantite != 0 && idannonce != null) {
			
			instAnnonce = new InstanceAnnonce();
			nextView = instAnnonce.changeAnnonceBDD(titre, niveau, isbn, annee, editeur, prix, quantite, remise, idannonce);
		}
		
		//si les attributs récupérés sont invalides, on renvoi vers la page de formulaire, permettant à l'utilisateur de rerenseigner l'annonce
		else { nextView = addAnnonce;}
		
		dispatcher = request.getRequestDispatcher(nextView);
		dispatcher.forward(request, response);
		
	}

}
