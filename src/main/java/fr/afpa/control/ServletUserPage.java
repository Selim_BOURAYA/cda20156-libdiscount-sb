package fr.afpa.control;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;

import fr.afpa.beans.Annonce;
import fr.afpa.constants.Constants;
import fr.afpa.model.InstanceAnnonce;
import fr.afpa.model.InstanceUser;

/**
 * Servlet implementation class ServletMainPage
 */
public class ServletUserPage extends HttpServlet implements Constants {
	private static final long serialVersionUID = 1L;
	private String nextView;
	private InstanceAnnonce instAnnonce;
	private ArrayList<Annonce> listeAnnonce;
	private String idAnnonce;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServletUserPage() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @author Selim récupération de l'id de l'annonce afin de la fairte transiter
	 *         vers la couche métier puis dao, récupérer les champs et permettre
	 *         leur modification
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		idAnnonce = request.getParameter("id");

		instAnnonce = new InstanceAnnonce();
		listeAnnonce = instAnnonce.getSelectAnnonce(idAnnonce);

		request.setAttribute("idAnnonce", idAnnonce);
		request.setAttribute("listeAnnonce", listeAnnonce);
		nextView = modifAnnonce;

		RequestDispatcher dispatcher = request.getRequestDispatcher(nextView);
		dispatcher.forward(request, response);

	}

	/**@Author Selim
	 * récupérere les valeurs des boutons de l'interface utilisateur afin de le rediriger vers les pages souhaitées
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//bouton "accueil"
		if (request.getParameter("accueil") != null) {

			nextView = mainPageUser;

			RequestDispatcher dispatcher = request.getRequestDispatcher(nextView);
			dispatcher.forward(request, response);

		}
		
		if (request.getParameter("authentification") != null) {
			
		nextView = authentification;
			
		RequestDispatcher dispatcher = request.getRequestDispatcher(nextView);
		dispatcher.forward(request, response);
			
		}
		
		// bouton "ajouter un utilisateur"
		if (request.getParameter("addUser") != null) {

			nextView = addUser;

			RequestDispatcher dispatcher = request.getRequestDispatcher(nextView);
			dispatcher.forward(request, response);

		}
		
		//bouton "ajouter une annonce"
		if (request.getParameter("addAnnonce") != null) {
			
			nextView = addAnnonce;

		}

		// bouton "modifier une annonce"
		if (request.getParameter("changeAnnonce") != null) {
			
			nextView = modifAnnonce;
		
		}
		
		
		// bouton "lister les annonces"
		if (request.getParameter("findAnnonces") != null) {
			
			nextView = triAnnonce;
			

		}
		
		
		RequestDispatcher dispatcher = request.getRequestDispatcher(nextView);
		dispatcher.forward(request, response);

	}

}
