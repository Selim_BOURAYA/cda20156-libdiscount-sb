package fr.afpa.control;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.beans.Annonce;
import fr.afpa.constants.Constants;
import fr.afpa.model.InstanceAnnonce;
import fr.afpa.model.InstanceUser;

/**
 * Servlet implementation class ServletUserAnnonce
 */
public class ServletUserAnnonce extends HttpServlet implements Constants {
	private static final long serialVersionUID = 1L;
	private InstanceAnnonce instAnnonce;
	private ArrayList<Annonce> listeAnnonce;
	private ArrayList<Annonce> listeFilterAnnonce;
	private String nextView;
	private RequestDispatcher dispatcher;
	private String id;
	private String filtre;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServletUserAnnonce() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**@author Selim
	 * récupére et fait transiter le mot clés renseigné par l'utilisateur, afin de faire remonter les annonces correspondantes
	 * et l'id utilisateur afin de récupérer la liste d'annonce de l'utilisateur
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		id = request.getParameter("id");
		filtre = request.getParameter("filtre");
		
		// récupération des annonces de l'utilisateur
		if (id != null) {
			
			instAnnonce = new InstanceAnnonce();
			listeAnnonce = instAnnonce.getOwnAnnonce(id);
			
			request.setAttribute("listeAnnonce", listeAnnonce);
			nextView = listUserAnnonce;
		}

		// récupération des annonces en fonction du mot clé
		else if (filtre != null) {
			
			instAnnonce = new InstanceAnnonce();
			listeFilterAnnonce = instAnnonce.getSelectAnnonce(filtre);
						
			request.setAttribute("listeFilterAnnonce", listeFilterAnnonce);
			nextView = annonceFiltrees;
		}

		dispatcher = request.getRequestDispatcher(nextView);
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		
		filtre = request.getParameter("filtre");
		nextView = annonceFiltrees;
			
		instAnnonce = new InstanceAnnonce();
		listeFilterAnnonce = instAnnonce.getSelectAnnonce(filtre);
		
		request.setAttribute("listeFilterAnnonce", listeFilterAnnonce);

		dispatcher = request.getRequestDispatcher(nextView);
		dispatcher.forward(request, response);

	}

}
