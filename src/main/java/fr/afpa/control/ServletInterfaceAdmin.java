package fr.afpa.control;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import fr.afpa.constants.Constants;

/**
 * Servlet implementation class ServletInterface
 */
public class ServletInterfaceAdmin extends HttpServlet implements Constants {
	private static final long serialVersionUID = 1L;
	private String nextView;
	private RequestDispatcher dispatcher;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletInterfaceAdmin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	}

	/**@author Selim
	 * retour vers la page d'accueil à partir du bouton accueil
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	if (request.getParameter("accueil") != null) {
			
			nextView = mainPageAdmin;
		}
	
	dispatcher = request.getRequestDispatcher(nextView);
	dispatcher.forward(request, response);
	
	}

}
