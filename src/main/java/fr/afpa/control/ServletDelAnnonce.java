package fr.afpa.control;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import fr.afpa.model.InstanceAnnonce;
import fr.afpa.model.InstanceUser;

/**
 * Servlet implementation class ServletDelAnnonce
 */
public class ServletDelAnnonce extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private RequestDispatcher dispatcher;
	private int idAnnonce;
	private InstanceAnnonce instAnnonce;
	private String nextView;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletDelAnnonce() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**@author Selim
	 * grace à l'idannonce, récupérée depuis la page .jsp, l'annonce est supprimée de la base de données
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		idAnnonce = Integer.parseInt(request.getParameter("id"));
		
		instAnnonce = new InstanceAnnonce();
		nextView = instAnnonce.deleteAnnonce(idAnnonce);
				
		dispatcher = request.getRequestDispatcher(nextView);
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
