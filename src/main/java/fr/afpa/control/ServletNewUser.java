package fr.afpa.control;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.constants.Constants;
import fr.afpa.model.InstanceUser;



/**
 * Servlet implementation class ServletAddUser
 */
public class ServletNewUser extends HttpServlet implements Constants {
	private static final long serialVersionUID = 1L;
	private String nom;
	private String prenom;
	private String librairie;
	private String adresse;
	private String codePostal;
	private String ville;
	private String mail;
	private String telephone;
	private String login;
	private String password;
	private String role;
	private InstanceUser instUser;
	private RequestDispatcher dispatcher;
	private String nextView;
	
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletNewUser() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**@author Selim
	 * Méthode récupérant les informations de l'utilisateur (depuis la page de premiere connection)
	 * ou par l'administrateur, depuis son interface, afin de les faire transiter vers la couche métier
	 * et instancier ce nouvel utilisateur
	 *
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		nom = request.getParameter("nom");
		prenom = request.getParameter("prenom");
		librairie = request.getParameter("librairie");
		adresse = request.getParameter("adresse");
		codePostal = request.getParameter("codePostal");
		ville = request.getParameter("ville");
		mail = request.getParameter("mail");
		telephone = request.getParameter("telephone");
		login = request.getParameter("login");
		password = request.getParameter("password");
		role = "user";
							
		// si le controle est respecte, on instancie un objet User qu'on enregistre dans une liste grace a la methode buildList()
		if (nom != null && prenom != null && librairie != null && adresse != null && codePostal != null && ville != null && mail != null &&
				telephone != null && login != null && password != null && role != null) {

			instUser = new InstanceUser();
			nextView = instUser.buildUserBDD(nom, prenom, librairie, adresse, codePostal, ville, mail, telephone, login, password, role);
		}
		
		else { nextView = errorPage;}
		
		dispatcher = request.getRequestDispatcher(nextView);
		dispatcher.forward(request, response);
	}

}
