package fr.afpa.model;

import java.util.ArrayList;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.User;
import fr.afpa.constants.Constants;
import fr.afpa.dao.CommBDD;

public class InstanceAnnonce implements Constants{
	private CommBDD talkBDD;	
	private Annonce annonce;
	private ArrayList <Annonce> listeAnnonce;
	private String etat;
	private int id;
	
	/**
	 * @author selim
	 * methode permettant d'inistancier un objet Annonce et de l'envoyer vers la couche DAO
	 */
	public ArrayList <Annonce> getSelectAnnonce(String entry) {
		
		talkBDD = new CommBDD();
		listeAnnonce = talkBDD.getSelectedAnnonceBDD(entry);
		
		return listeAnnonce;
	}
		
	/**
	 * @author selim
	 * methode permettant de récupérer et de lister l'ensemble des annonces enregistrées dans la BDD
	 */
	public ArrayList <Annonce> getAllAnnonce() {
		
		talkBDD = new CommBDD();
		listeAnnonce = talkBDD.getAllAnnonceBDD();	
		
		return listeAnnonce;
	}
	/**
	 * 
	 * @author selim
	 * methode permettant de lister les annonces de l'utilisateur connecté
	 */
	public ArrayList <Annonce> getOwnAnnonce(String id) {
		
		talkBDD = new CommBDD();
		listeAnnonce = talkBDD.getOwnAnnonceBDD(id);
		
		return listeAnnonce;
	}

	/**
	 * @author selim
	 * methode permettant d'inistancier un objet Annonce et de l'envoyer vers la couche DAO
	 */
	public String buildAnnonceBDD(String titre, String niveau, String isbn, String annee, 
			String editeur, int prix, int quantite, int remise) {

		talkBDD = new CommBDD();
		annonce = new Annonce(titre, niveau, isbn, annee, editeur, prix, quantite, remise);
	
		if(annonce != null) {
		talkBDD.createAnnonce(annonce);
		return addAnnonceSucceed;
		}
		else { return addAnnonce;}
	}
	
	/**
	 * @author selim
	 * @param les nouvelles informations, renseignées depuis l'interface "modifier une annonce" , 
	 * sont renvoyés afin de remplacer l'annonce originale
	 * 
	 */
	public String changeAnnonceBDD(String titre, String niveau, String isbn, String annee, 
			String editeur, int prix, int quantite, int remise, String idannonce) {
		
		id = Integer.parseInt(idannonce);
				
		talkBDD = new CommBDD();
		annonce = new Annonce(titre, niveau, isbn, annee, editeur, prix, quantite, remise);
		
		talkBDD.replaceAnnonce(annonce, id);
		
		return addAnnonceSucceed;
	}
	
	/**
	 * @author selim
	 * methode permettant d'appeler la méthode setInactiveAnnonce(), qui intervertit l'etat de l'annonce
	 * entre "active" et "inactive" en fonction de son état initial, et récupère son état.
	 * En fonction de ce dernier, l'application renvoit une page confirmant l'activation ou la désactivation
	 * @return
	 */
	public String deactivateAnnonce(int idAnnonce, String role) {
		
		talkBDD = new CommBDD();
		etat = talkBDD.setInactiveAnnonce(idAnnonce);
				
		if(etat.contentEquals("inactive") && role == null) {
			return deactivatedSucceed;
		}
		
		else if(etat.contentEquals("inactive") && role.contentEquals("user")) {
		return AnnonceUserDeactivated;
		}
		
		else if (etat.contentEquals("active") && role == null) {
			return activatedSucceed; }
		
		else if (etat.contentEquals("active") && role.contentEquals("user")) {
			return AnnonceUserActivated; }
		
		return null;
	}
	
	/**
	 * @author selim
	 * methode permettant d'appeler la méthode setInactiveAnnonce(), qui intervertit l'etat de l'annonce
	 * entre "active" et "inactive" en fonction de son état initial, et récupère son état.
	 * En fonction de ce dernier, l'application renvoit une page confirmant la suppression de l'annonce
	 * @return
	 */
	public String deleteAnnonce(int idAnnonce) {
		
		talkBDD = new CommBDD();
		talkBDD.deleteAnnonceBDD(idAnnonce);
				
		return deactivatedSucceed;
	}

}



