package fr.afpa.model;

import java.util.ArrayList;

import fr.afpa.beans.User;
import fr.afpa.constants.Constants;
import fr.afpa.dao.CommBDD;
import lombok.Getter;

@Getter
public class InstanceUser implements Constants {

	private User loggedUser;
	private ArrayList <User> listUser;
	private User user;
	private CommBDD talkBDD;
	private String nextView;
	private String statut;

	/**
	 * @author selim 
	 * Methode permettant l'ouverture de la page d'accueil après la validation des identifiants
	 */
		public String CheckUserBDD(String login, String password) {

			talkBDD = new CommBDD();
			
			if (talkBDD.checkValidUser(login, password) == true) {
				
				loggedUser = getUser(login);
											
				if(loggedUser.getRole().contains("user")) {
				nextView = mainPageUser;
				
				}
				else { 	nextView = mainPageAdmin;}
				return nextView;
			}
			
			nextView = errorPage;
			return nextView;
		}
		
	/**
	 * @author selim
	 * Instanciation d'un User à partir de son login
	 * L'appel de la méthode "getUser" permet de récupérer les informations de l'utilisateur correpondant depuis la BDD
	 */
	public User getUser(String login) {

		talkBDD = new CommBDD();
		
		loggedUser = talkBDD.getUserBDD(login);
				
		return loggedUser;
	}
	
	public ArrayList <User> getListUser() {

		talkBDD = new CommBDD();
		
		listUser = talkBDD.getListUserBDD();
		
		return listUser;
	}
	
	/**
	 * author selim
	 * appel de la méthode changeStateUser, permettant de passer le statut de l'utilisateur en inactif
	 * Une fois le changement effectué, la méthode renverra une page de succès
	 * @return
	 */
	public String deactivatedUser(String login) {
		
		talkBDD = new CommBDD();
		statut = talkBDD.setInactiveUser(login);
				
		if(statut.contentEquals("inactif")) {
		return deactivatedSucceed;
		}
		
		if (statut.contentEquals("actif")) {
			return activatedSucceed; }
		
		return null;
	}
		
	/**	
	 * @author Selim 
	 * Instanciation d'un nouvel User à partir des informations de la couche controle
	 *  L'objet user sera ensuite transmis au service "createUser" de la couche BDD et la méthode renverra une page "succes"
	 */
	public String buildUserBDD(String nom, String prenom, String librairie, String adresse, String codePostal, String ville, 
			String mail, String telephone, String login, String password, String role) {

		talkBDD = new CommBDD();
		user = new User(nom, prenom, librairie, adresse, codePostal, ville, mail, telephone, login, password, role);
		
		if(user != null && role.contentEquals("admin")) {
		talkBDD.createUser(user);
		return addUserSucceed;
		}
		
		else if(user != null && role.contentEquals("user")) {
			talkBDD.createUser(user);
			return "index.jsp";
		}
			
		else {return errorPage;}
	}
	
	
}
