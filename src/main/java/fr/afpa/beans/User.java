package fr.afpa.beans;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString

@Entity(name = "Userlib")
@NamedQuery(name = "findByLogin", query = "select user from Userlib user where user.login = :login") 
@NamedQuery(name = "findByRole", query = "select user from Userlib user where user.role = :role") 
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_id_gen")
	@SequenceGenerator(name="user_id_gen", sequenceName= "seq_user", allocationSize = 1)
	@Column (name = "iduser")
	private int iduser;
		
	@Column(name = "nom")
	private String nom;
	
	@Column(name = "prenom")
	private String prenom;

	@Column(name = "librairie")
	private String librairie;
	
	@Column(name = "adresse")
	private String adresse;
	
	@Column(name = "codePostal")
	private String codePostal;
	
	@Column(name = "ville")
	private String ville;
			
	@Column(name = "mail", unique=true)
	private String mail;
	
	@Column(name = "telephone")
	private String telephone;
	
	@Column(name = "login", unique=true)
	private String login;
	
	@Column(name = "password")
	private String password;
	
	@Column(name = "role")
	private String role;
	
	@Column(name = "statut")
	private String statut;
		
		
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
	private List <Annonce> ListAnnonce;

	public User(String nom, String prenom, String librairie, String adresse, String codePostal, String ville, 
			String mail, String telephone, String login, String password, String role) {
				
		this.nom = nom;
		this.prenom = prenom;
		this.librairie = librairie;
		this.adresse = adresse;
		this.codePostal = codePostal;
		this.ville = ville;
		this.mail = mail;
		this.telephone = telephone;
		this.login = login;
		this.password = password;
		this.role = role;
		this.statut = "actif";
	}
	
	
	
}	

	