package fr.afpa.beans;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor


@Entity(name = "Annonce")
@NamedQuery(name = "findById", query = "select annonce from Annonce annonce where annonce.idannonce = :idannonce") 
@NamedQuery(name = "getAllAnnonces", query = "select annonce from Annonce annonce") 

public class Annonce {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "annonce_id_generator")
	@SequenceGenerator(name="annonce_id_generator", sequenceName = "seq_annonce", allocationSize = 1)
	@Column (name = "idannonce")
	private int idannonce;
	
	@Column(name = "titre")
	private String titre;
	
	@Column(name = "niveau")
	private String niveau;
	
	@Column(name = "isbn")
	private String isbn;
	
	@Column(name = "annee")
	private String annee;
	
	@Column(name = "editeur")
	private String editeur;
	
	@Column(name = "prix")
	private int prix;
	
	@Column(name = "quantite")
	private int quantite;
	
	@Column(name = "remise")
	private int remise;
	
	@Column(name = "etat", columnDefinition = "varchar(255) default 'active'")
	private String etat;
			
	@ManyToOne (cascade = CascadeType.MERGE)
	@JoinColumn (name = "fk_iduser", referencedColumnName = "iduser")
	private User user;
	
	public Annonce(String titre, String niveau, String isbn, String annee, String editeur, int prix,
			int quantite, int remise) {
		
		this.titre = titre;
		this.niveau = niveau;
		this.isbn = isbn;
		this.annee = annee;
		this.editeur = editeur;
		this.prix = prix;
		this.quantite = quantite;
		this.remise = remise;
		this.etat = "active";
		
	}
	
	
	@Override
	public String toString() {
		return titre;
	}





}


