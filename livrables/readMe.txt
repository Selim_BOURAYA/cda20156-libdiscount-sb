
############                    LeBonLibraire                     ##############

	v.1
	15 novembre 2020
	par Selim


Lien GitHuB du projet :

https://bitbucket.org/selim_bouraya/cda20156-libdiscount-sb.git


Bienvenue sur le site LeBonLibraire !

Celle-ci va vous permettre de créer un compte et de mettre en vente les livres que vous souhaitez.
Ceux-ci seront ensuite consultable par les autres utilisateurs.
Vous pourrez à tout moment consulter et trier les annonces présente dans notre base de données, consulter,
archiver et modifier vos annonces.

Si vous disposer des droits administateur, vous aurez la possibilité de consulter les listes d'utilisateurs et d'annonces 
et de l'administer.


Avant de lancer l'application, assurez vous de placer le fichier .war à la racine du dossier webapps de votre serveur TomCat

Il sera de plus nécessaire, pour générér la base de données postgreSQl.
Pour cela, il est necessaire de créer, via pgAdmin, une base de donnée  ;
		
	nom de la base de donnée : CDALibrairie
	port local : 5432
	
et un utilisateur ;
	
	username : user
	password : Admin
	
Le script Script-Librairie_User.sql, présent dans le dossier Script SQL	contenu dans le dossier livrables, peut être executé 
depuis DBeaver une fois la base générée lors du premier lancement.


Interface d'authentification:

L'utilisateur doit entrer son login et son mot de passe.

Si l'utilisateur se connecte pour la première fois, il doit créer un compte en cliquant sur le bouton nouvel utilisateur et renseigner l'ensemble des champs.
Si le mail et le login sont déja présent dans la base de données, celle-ci renverra une erreur (ajout d'un contrôle nécessaire)

Une fois le nouvel utilisateur créé, un retour à l'écran d'authentification est effectué, et l'utilisateur doit rentrer son login et son mot de passe



Interface utilisateur :

Une fois l'authentification réussie et l'utilisateur connecté à la base de données, l'utilisateur peut ;
	
	
	- Consulter ses annonces :  affichage des annonces postées par l'utilisateur. Possibilité de modification, d'activation et désactivation de l'annonce via les liens à droite de l'annonce
		bug : le changement d'état (actif/inactif/actif) ou (inactif/actif/inactif) n'est possible qu'une seule fois.
	
	- Ajouter une annonce : Permet à l'utilisateur d'ajouter une annonce. 
		Les champs prix, quantité et remise doivent être renseignés avec des nombres (remise comprise entre 0 et 100%, le contrôle des entrées sera mis en oeuvre lors d'une prochaine MAJ)
		L'enregistrement est validé après l'appui du bouton "soumettre la requête"
	
	- Rechercher des annonces par mot clé (Titre, année, niveau, editeur, ville). Valider la recherche avec le bouton "rechercher"
	
	- Se déconnecter
	

	
Interface Administrateur :

Le ou les administrateurs auront quant à eux accès à une autre interface permettant de ;

	- ajouter un utilisateur : identique à l'interface de création d'utilisateur accessible depuis l'écran d'authentification, 
		l'administraeur peut ici décider du rôle et droit donné à l'utilisateur (admin ou user)
		Si le mail et le login sont déja présent dans la base de données, celle-ci renverra une erreur (ajout d'un contrôle nécessaire)
		
	- Afficher et supprimer une annonce : cette fonctionnalité permet de lister l'ensemble des annonces présente dans la base de données
		l'administrateur peut alors activer, désactiver ou supprimer l'annonce en cliquant sur le lien correspondant, à droite de l'annonce
		Il est possible à tout moment de revenir à l'accueil grâce au bouton "Retour à l'accueil"
		
	- 	Gérer les utilisateurs : permet de lister, activer et désactiver les utilisateurs
		bug : le changement d'état (actif/inactif/actif) ou (inactif/actif/inactif) n'est possible qu'une seule fois.

