------------------------------------------------
--INSERTION D'UTLISATEURS
------------------------------------------------

insert into Userlib 
values 
(nextval('seq_user'), '10 rue Aimee','18600', 'Voyage de nuit', 'Mike','barque@mail.fr', 'DENIS','1234','Michel','admin','actif','0760069012', 'PARIS' ),
(nextval('seq_user'), '1 rue Liberte','80000', 'Caracole', 'Dan','simone@mail.fr', 'SERIE','1234','Dany','user','actif','0662269015', 'AMIENS' ),
(nextval('seq_user'), '9 rue Paul Vaillant Couturier','60110', 'test', 'test','test@mail.fr','BOU','test','Zo' ,'user','actif','0798579073', 'AIRAINES' ),
(nextval('seq_user'), '9 rue Paul Vaillant Couturier','60110', 'Se', 'Selim','selim@mail.fr','BOU','se','Selim' ,'admin','actif','0798579073', 'MERU' ),
(nextval('seq_user'), '7 rue des acacias','60000', 'Ligniere', 'Mao','tron@mail.fr', 'BOU','1507','Mohamed','user','actif','0645349012', 'MERU' );


------------------------------------------------
--INSERTION Annonces
------------------------------------------------
insert into Annonce
values
(nextval('seq_annonce'), '1987','Lux','active', ' 978-2-7654-0912-0 ','IV',25,1,20,'Mort a credit', 1),
(nextval('seq_annonce'), '1992','test','active', ' 978-1-9803-0956-2 ','III',20,3,25,'Mort a credit II', 3),
(nextval('seq_annonce'), '1984','Lux','active', ' 978-2-8734-0912-0 ','I',15,1,15,'Jack le Fataliste', 1),
(nextval('seq_annonce'), '1992','Baak','active', ' 978-2-9813-1234-4 ','IV',15.50,1,0,'1984', 2),
(nextval('seq_annonce'), '1962','Baak','active', ' 978-4-7654-9967-0 ','II',17.90,2,0,'La ferme des animaux', 4);



